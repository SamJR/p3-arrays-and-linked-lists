//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Samuel Reis on 13/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef LinkedList_hpp
#define LinkedList_hpp

#include <stdio.h>

class LinkedList
{
public:
    LinkedList();
    ~LinkedList();
    void add (float itemValue);
    float get (int index);
    int size() const;
private:
    int listSize;
    struct Node
    {
        float value;
        Node* next;
    };
    Node* h;
    Node* n;
    Node* t;
    Node* addT;
};

#endif /* LinkedList_hpp */
