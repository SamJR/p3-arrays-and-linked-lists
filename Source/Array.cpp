//
//  Array.cpp
//  CommandLineTool
//
//  Created by Samuel Reis on 10/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "Array.hpp"

//Initialise Variables
Array::Array()
{
    arraySize = 0;
    floatPoint = nullptr;
}

/* When the class goes out of scope, check that floatpoint is not pointingto anything.
 If it isn't, remove the floatpoint memory allocation.*/
Array::~Array()
{
    if (floatPoint != nullptr)
    {
        delete[] floatPoint;
    }
}

/* Adds an value onto the end of the array and increases arraysize by 1 */
void Array::add (float itemValue)
{
    //declare new array which is 1 bigger than the old array
    float* tempPtr = new float[arraySize + 1];
    
    //copy contents of old array into new array
    for (int i = 0; i < arraySize; i++)
        tempPtr[i] = floatPoint[i];
    
    //Add itemValue into thelastslot of the array
    tempPtr[arraySize] = itemValue;
    
    //If floatpoint is pointing to something then delete floatpoint
    if (floatPoint != nullptr)
        delete[] floatPoint;
    
    //copy temp values back into the old array
    floatPoint = tempPtr;
    
    //Increase array size by 1
    arraySize++;
}

/* If the index is greater than or equal to 0 and smaller than the array size then return the index value.
 Else return 0 */
float Array::get (int index) const
{
    if (index >= 0 && index < size())
        return floatPoint[index];
    else
        return 0.f;
}

//Return ArraySize
int Array::size() const
{
    return arraySize;
};