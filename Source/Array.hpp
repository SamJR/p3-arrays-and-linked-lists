//
//  Array.hpp
//  CommandLineTool
//
//  Created by Samuel Reis on 10/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>

class Array
{
public:
    Array();
    ~Array();
    void add (float itemValue);
    float get (int index) const;
    int size() const;
private:
    int arraySize;
    float *floatPoint = nullptr;
};

#endif /* Array_hpp */